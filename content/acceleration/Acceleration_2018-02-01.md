---
title: "Thursday Acceleration Rankings"
date: 2018-02-01T16:20:47.113
draft: false
---
### Top Decile Acceleration w/ Positive Momentum
{{< acc-csv url = "data/2018-02-01/Dual_Top_Regress.csv" >}}
<hr>
{{< download href="/data/2018-02-01/Acceleration_Regression_Full.csv" download="Acceleration_Regression_Full.csv" text="Download Regression CSV" >}}
<hr>
{{< download href="/data/2018-02-01/Acceleration_Simple_Full.csv" download="Acceleration_Simple_Full.csv" text="Download Simple CSV" >}}
