---
title: "Accleration Rankings"
date: 2017-03-02T12:00:00-05:00
---
This is the acceleration rankings navigation page

This section will provide candidates for buying long OTM calls on stocks with anticipated positive drift.

**Methodology:**

1. Get last 100 days of log returns

2. Perform regression on data with 42 window. For regression, multiply slopeby Rsquared value. This is adjusted slope / momentum score. Inspired by Andreas Clenow.

3. Compare momentum rankings from present vs 21 trading days ago. Difference between these two values is acceleration score.

4. Sort by descending acceleration score.

5. Take top decile stocks.

6. Remove any stocks that do not currently have positive momentum.
