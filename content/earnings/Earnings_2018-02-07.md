---
title: "Wednesday Earnings Strategy"
date: 2018-02-07T07:35:04.334
draft: false
---
### High Threshold Candidates
{{< get-csv url = "data/2018-02-07/High.csv" >}}
<hr>
### Low Threshold Candidates
{{< get-csv url="/data/2018-02-07/Low.csv" >}}
<hr>
{{< download href="/data/2018-02-07/AllEarnings.csv" download="All.csv" text="Download All Upcoming Earnings" >}}
