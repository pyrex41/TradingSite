---
title: "Friday Earnings Strategy"
date: 2018-01-26T07:35:56.521
draft: false
---
### High Threshold Candidates
{{< get-csv url = "data/2018-01-26/High.csv" >}}
<hr>
### Low Threshold Candidates
{{< get-csv url="/data/2018-01-26/Low.csv" >}}
<hr>
{{< download href="/data/2018-01-26/AllEarnings.csv" download="All.csv" text="Download All Upcoming Earnings" >}}
