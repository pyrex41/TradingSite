---
title: "Thursday Earnings Strategy"
date: 2018-01-25T07:35:47.291
draft: false
---
### High Threshold Candidates
{{< get-csv url = "data/2018-01-25/High.csv" >}}
<hr>
### Low Threshold Candidates
{{< get-csv url="/data/2018-01-25/Low.csv" >}}
<hr>
{{< download href="/data/2018-01-25/AllEarnings.csv" download="All.csv" text="Download All Upcoming Earnings" >}}
