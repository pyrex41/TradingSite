---
title: "Friday Earnings Strategy"
date: 2018-02-09T07:37:39.59
draft: false
---
### High Threshold Candidates
{{< get-csv url = "data/2018-02-09/High.csv" >}}
<hr>
### Low Threshold Candidates
{{< get-csv url="/data/2018-02-09/Low.csv" >}}
<hr>
{{< download href="/data/2018-02-09/AllEarnings.csv" download="All.csv" text="Download All Upcoming Earnings" >}}
