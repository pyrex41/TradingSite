---
title: "Tuesday Earnings Strategy"
date: 2018-01-16T07:35:04.301
draft: false
---
### High Threshold Candidates
{{< get-csv url = "data/2018-01-16/High.csv" >}}
<hr>
### Low Threshold Candidates
{{< get-csv url="/data/2018-01-16/Low.csv" >}}
<hr>
{{< download href="/data/2018-01-16/AllEarnings.csv" download="All.csv" text="Download All Upcoming Earnings" >}}
