---
title: "Overview: Austrian Investing II"
date: 2017-11-12T11:10:16-05:00
draft: false
---
From "Dao of Capital". Uses downloaded financials using RCall via Julia. Current stores each in csv in a nested folder structure, need to migrate to a proper database. Then can lookup candidates based on Spetznagle's framework.

Could also easily adapt to other kinds of fundamental screens (eg, Magic Formula). Could even build a custom analytical tool to let users build their own fundamental screens.
