---
title: "Sunday Vol Strategy"
date: 2018-01-28T12:01:34.126
draft: false
---
### Mojito 3.0 Indicator
Today's smooth CM45 IVTS signal is 0.925. Strategy signal: *flat*.
<br>
#### Recent CM45
{{< myimg src="/images/2018-01-28/cm45_recent.png" >}}
<br>
<hr>
<br>
### Daily Roll Indicator
Today's Daily Roll is -0.042. Strategy signal: *flat*.
<br>
#### Recent Daily Roll
{{< myimg src="/images/2018-01-28/daily_roll_recent.png" >}}
<hr>
{{< download href="/data/2018-01-28/Recent_Data.csv" download="Recent.csv" text="Download Recent Data" >}}
<br>
{{< download href="/data/2018-01-28/Full_Data.csv" download="Full.csv" text="Download Full Data" >}}
