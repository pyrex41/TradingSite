---
title: "Wednesday Vol Strategy"
date: 2018-01-31T07:02:16.682
draft: false
---
### Mojito 3.0 Indicator
Today's smooth CM45 IVTS signal is 0.908. Strategy signal: *short*.
<br>
#### Recent CM45
{{< myimg src="/images/2018-01-31/cm45_recent.png" >}}
<br>
<hr>
<br>
### Daily Roll Indicator
Today's Daily Roll is 0.018. Strategy signal: *flat*.
<br>
#### Recent Daily Roll
{{< myimg src="/images/2018-01-31/daily_roll_recent.png" >}}
<hr>
{{< download href="/data/2018-01-31/Recent_Data.csv" download="Recent.csv" text="Download Recent Data" >}}
<br>
{{< download href="/data/2018-01-31/Full_Data.csv" download="Full.csv" text="Download Full Data" >}}
