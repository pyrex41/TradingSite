---
title: "Sunday Vol Strategy"
date: 2018-02-04T13:31:30.059
draft: false
---
### Mojito 3.0 Indicator
Today's smooth CM45 IVTS signal is 0.911. Strategy signal: *flat*.
<br>
#### Recent CM45
{{< myimg src="/images/2018-02-04/cm45_recent.png" >}}
<br>
<hr>
<br>
### Daily Roll Indicator
Today's Daily Roll is 0.015. Strategy signal: *flat*.
<br>
#### Recent Daily Roll
{{< myimg src="/images/2018-02-04/daily_roll_recent.png" >}}
<hr>
{{< download href="/data/2018-02-04/Recent_Data.csv" download="Recent.csv" text="Download Recent Data" >}}
<br>
{{< download href="/data/2018-02-04/Full_Data.csv" download="Full.csv" text="Download Full Data" >}}
