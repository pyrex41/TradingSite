---
title: "Thursday Vol Strategy"
date: 2017-11-30T07:01:35.407
draft: false
---
### Mojito 3.0 Indicator
Today's smooth CM45 IVTS signal is 0.823. Strategy signal: *short*.
<br>
#### Recent CM45
{{< myimg src="/images/2017-11-30/cm45_recent.png" >}}
<br>
<hr>
<br>
### Daily Roll Indicator
Today's Daily Roll is -0.031. Strategy signal: *flat*.
<br>
#### Recent Daily Roll
{{< myimg src="/images/2017-11-30/daily_roll_recent.png" >}}
<hr>
{{< download href="/data/2017-11-30/Recent_Data.csv" download="Recent.csv" text="Download Recent Data" >}}
<br>
{{< download href="/data/2017-11-30/Full_Data.csv" download="Full.csv" text="Download Full Data" >}}
