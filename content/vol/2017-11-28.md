---
title: "Tuesday Vol Strategy"
date: 2017-11-28T15:04:13.019
draft: false
---
### Mojito 3.0 Indicator
Today's smooth CM45 IVTS signal is 0.852. Strategy signal: *short*.
<br>
#### Recent CM45
{{< myimg src="/images/2017-11-28/cm45_recent.png" >}}
<br>
<hr>
<br>
### Daily Roll Indicator
Today's Daily Roll is -0.062. Strategy signal: *flat*.
<br>
#### Recent Daily Roll
{{< myimg src="/images/2017-11-28/daily_roll_recent.png" >}}
<hr>
{{< download href="/data/2017-11-28/Recent_Data.csv" download="Recent.csv" text="Download Recent Data" >}}
<br>
{{< download href="/data/2017-11-28/Full_Data.csv" download="Full.csv" text="Download Full Data" >}}
