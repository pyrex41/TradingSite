---
title: "Volatility"
date: 2017-03-02T12:00:00-05:00
---
This is the list navigation page for the volatility section.

This strategy provides indicators for going short VXX/UVXY in some fashion.

See [Mojito 3.0](http://godotfinance.com/pdf/DynamicVIXFuturesVersion3.pdf) for more info on indicators

My preferred approach will likely to buy longer dated UVXY puts, selling weekly puts against it.

Another [relevant paper](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2871616) on trading volatility term structure.
