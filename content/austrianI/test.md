---
title: "Overview: Austrian Investing I"
date: 2017-11-12T11:05:39-05:00
draft: false
---
Uses Tobin's Q to predict liklihood of financial "crash" and to provide indicator for weather to buy OTM puts on market. From "Dao of Capital".

[More info](http://www.universa.net/UniversaSpitznagel_research_20110613.pdf)
