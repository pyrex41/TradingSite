---
title: "Trading Tools and Indicators"
featured_image: '/images/wave.jpg'
description: "Tools and indicators to streamline trading decisions and workflow."
---
Welcome to my trading webpage. I have different sections with automated updates for different indicators. If you have ideas for further functionality, feel free to reach out to me at [reub.brooks@gmail.com](mailto:reub.brooks@gmail.com).
